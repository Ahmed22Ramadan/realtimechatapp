<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ChatRoom;

class ChatRoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChatRoom::create(['name'=>"chatRoom.1"]);
        ChatRoom::create(['name'=>"chatRoom.2"]);
    }
}
