<?php

namespace App\Models;

use App\Models\User;
use App\Models\ChatRoom;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ChatMessages extends Model
{
    use HasFactory;

    protected $table = "chat_messages";
    protected $fillable = ["message","user_id","receiver_id","unreaded_message"];

    public function user(){
        return $this->hasOne(User::class,"id","user_id");
    }
    
}
