<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ChatMessages;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Events\SendMessageEvent;

class ChatAppController extends Controller
{

    public function messages(Request $request)
    {
        return ChatMessages::where("user_id", Auth::id())
            ->where("receiver_id", $request->user_id)
            ->orWhere("receiver_id", Auth::id())
            ->Where("user_id", $request->user_id)
            ->with("user")
            ->orderBy("id", "asc")->get();
    }

    public function newMessage(Request $request)
    {
        $newMessage = ChatMessages::create(array_merge($request->only("message", "receiver_id"), ["user_id" => Auth::id()]));
        broadcast(new SendMessageEvent($newMessage))->toOthers();
        return $newMessage;
    }

    public function users()
    {
        return User::all()->except(Auth::id());
    }
    public function authorizedUser()
    {
        return Auth::user();
    }
    public function countUnreadedMessages(Request $request)
    {
        return ChatMessages::where("unreaded_message","1")
        ->where("receiver_id",Auth::id())
        ->where("user_id",$request->sender_id)
        ->count();
    }
    public function refreshMessagesCounter(Request $request)
    {
        ChatMessages::where("receiver_id",Auth::id())
        ->where("user_id",$request->user_id)
        ->update(["unreaded_message"=>"0"]);
        // return ChatMessages::where("user_id",$request->user_id)->orWhere("receiver_id",$request->user_id)->update(["unreaded_message"=>"0"]);
    }
}
