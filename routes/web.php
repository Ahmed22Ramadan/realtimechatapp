<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\ChatAppController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/chat', function () {
    return Inertia::render('Chat/container');
})->name('chat');

Route::group(["middleware"=>"auth:sanctum"],function(){
    Route::post("chat/rooms",[ChatAppController::class,"rooms"]);
    Route::post("chat/users",[ChatAppController::class,"users"]);
    Route::post("chat/user/messages",[ChatAppController::class,"messages"]);
    Route::post("chat/authorized/user",[ChatAppController::class,"authorizedUser"]);
    Route::post("chat/user/messages/message",[ChatAppController::class,"newMessage"]);
    Route::post("count/unreaded/messages",[ChatAppController::class,"countUnreadedMessages"]);
    Route::post("refresh/messages/counter",[ChatAppController::class,"refreshMessagesCounter"]);
    
    
});